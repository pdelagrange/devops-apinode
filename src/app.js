import express from "express";
import cors from "express";

const app = express();

var port = process.env.PORT || 3000;


// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// enable cors
app.use(cors());

app.get('/status', function (request, response) {
    response.status(200).send();
    response.end();
});

app.get('/city', function (request, response) {
    const city = ['Paris', 'Bordeaux', 'Lyon', 'Strasbourg', 'Toulouse', 'Marseille'];
    response.json(city);
    response.end();
});


app.use(function (req, res, next) {
    res.status(404).send('Unable to find the requested resource!');
    res.end();
});




app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
